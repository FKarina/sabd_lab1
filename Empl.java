package lab1;

import lombok.Data;

@Data
public class Empl {
    private String id;
    private String firstName;
    private String lastName;
    private String location;
}
